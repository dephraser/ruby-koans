# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(a, b, c)

        hyp, side1, side2 = if a > b and a > c
                                [a,b,c]
                            elsif b > c and b > a
                                [b,a,c]
                            else
                                [c,a,b]
                            end

        if a <= 0 or b <= 0 or c <= 0
            raise TriangleError
        end

        if side1 + side2 <= hyp
            raise TriangleError
        end


        triangle_type = if (a == b) and (b==c) and (a==c)
                            :equilateral

                        elsif (a==b) or (b==c) or (a==c)
                            :isosceles
                
                        else
                            :scalene

                        end
    
end


# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
